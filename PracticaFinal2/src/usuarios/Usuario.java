package usuarios;

public class Usuario {
	
	private String nombre;
	private String contrasena;
	private double saldo;
	private boolean administrador;
	
	public Usuario(){
		
	}
	public Usuario(String nombre,String contrasena,boolean admin,double saldo){
		this.nombre=nombre;
		this.contrasena=contrasena;
		this.administrador=admin;
		this.saldo=saldo;
		
	}
	
	public Usuario(String nombre,String contrasena,boolean admin){
		this.nombre=nombre;
		this.contrasena=contrasena;
		this.administrador=admin;
		
	}
	
	public Usuario(String nombre,String contrasena){
		this.nombre=nombre;
		this.contrasena=contrasena;
	}
	public void restarSaldo(int cantidadARestar){
		this.saldo=saldo-cantidadARestar;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public boolean isAdministrador() {
		return administrador;
	}
	public void setAdministrador(boolean administrador) {
		this.administrador = administrador;
	}


	@Override
	public String toString() {
		return "Usuario{" +
				"nombre='" + nombre + '\'' +
				", contrasena='" + contrasena + '\'' +
				", saldo=" + saldo +
				", administrador=" + administrador +
				'}';
	}
}
