package usuarios;

import java.util.ArrayList;

public class ListaUsuarios {
	
	private ArrayList<Usuario> listaUsuario;
	
	public ListaUsuarios() {
		listaUsuario = new ArrayList<Usuario>();
		anadirUsuarios();
	}
//A�adimos 2 usuarios para realizar las pruebas
	private void anadirUsuarios() {
		Usuario normal = new Usuario("juan","1234",false,40);
		Usuario admin = new Usuario("admin","1234",true,50);
		
		listaUsuario.add(admin);
		listaUsuario.add(normal);
	}
//getter y setter de el array de usuarios
	public ArrayList<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(ArrayList<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	
	public void anadirUsuario(String nombre,String contrasena){
		Usuario newUsuario = new Usuario(nombre,contrasena);
		listaUsuario.add(newUsuario);
	}
	
	
	

}
