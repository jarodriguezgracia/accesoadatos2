package vista;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Created by DAM on 23/10/2018.
 */
public class Vista {

     JTabbedPane tabbedPane1;
    private JPanel panel1;
    JTextField textNombreVehiculo;
    JTextField textTipoVehiculo;
    JTextField textDiseñoVehiculo;
    JTextField textPrecioVehiculo;
    JTextField textNombrePiloto;
    JTextField textTrofeosPiloto;
    JTextField textFechaPiloto;


    JButton ButtonAltaVehiculo;
    JButton ButtonEliminarVehiculo;
    JButton ButtonModificarVehiculo;
    JButton ButtonAltaPiloto;
    JButton ButtonEliminarPiloto;
    JButton buttonModificarPiloto;

    JMenuItem opcionGuardar;
    JMenuItem opcionCargar;
    JMenuItem opcionSalir;
    JMenuItem opcionExportarXML;
    JMenuItem opcionImportarXML;
    JMenuItem opcionGuardarDB;

    JButton btAltaCircuito;

    JButton btnEliminarCircuito;

     JComboBox comboBoxListaPilotos;
    DefaultComboBoxModel dlmcomboBoxPilotos;

    JTextField txtNombreCircuito;

    DefaultListModel dlmCircuitos;
    DefaultListModel dlmPilotos;
     JList listPilotosEnCircuito;
    JTextField txtMatriculaVehiculo;
    JTextField textLocalidadCircuito;
    JButton btModificarCircuito;
    JTable tablePilotos;
    JTable tableVehiculo;
    DatePicker txtFechaPiloto;
    JTextField txtIdPiloto;

    DefaultComboBoxModel dcbVehiculoPiloto;
    JComboBox comboBoxVehiculoPiloto;
    JTable tableCircuitos;
    private JTable tablePilotosEncircuito;
    DefaultTableModel dlmPilotosEnCirucito;
    JList JlistPilotos;
    JList JlistCiruitos;
    JButton añadirPilotosHaCircuitoButton;
    JTextField textContraseña;
     JTextField textUser;
     JButton accederButton;
    JPanel panelNewUser;
    JTextField textaddUser;
    JTextField textaddPassword;
    JButton añadiruser;
    private JPanel panelConfiguracion;
    JTextField textServer;
    JTextField textPuerto;
    JTextField textDB;
    JTextField textUserDB;
    JTextField textPasswordDB;
    JButton btnCambiarPreferencias;
    JRadioButton mySQLRadioButton;
    JRadioButton postGrepRadioButton;
     JCheckBox circuitosCheckBox;
     JCheckBox vehiculoCheckBox;
     JCheckBox pilotosCheckBox;
    JTextField textBusquedaGeneral;

     JTable tableBusquedaPilotos;
     JButton consultarCircuitosButton;
    JButton ConsultarCir;
    JButton crearPredeterminadaFinalButton;


     JTable tableBusquedaVehiculos;
     JTable tableBusquedaCircuitos;
     JComboBox comboBoxBusquedaPersonalizada;
     DefaultComboBoxModel dlmBusquedaPersonalizada;
    JTextField txtTextoBusquedaPerosnalizada;
    JTextField txtCampoAbuscar;
    DefaultTableModel dtmBusquedaPilotos;
    DefaultTableModel dtmBusquedaVehiculo;
    DefaultTableModel dtmBusquedaCircuito;


    DefaultTableModel defaultTableVehiculo;
    DefaultTableModel defaultTablePilotos;
    DefaultTableModel defaultTableCircuitos;

    JFrame frame;



    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        //frame.setResizable(false);
        frame.setVisible(true);

        defaultTableVehiculo = new DefaultTableModel();
        tableVehiculo.setModel(defaultTableVehiculo);

        defaultTablePilotos = new DefaultTableModel();
        tablePilotos.setModel(defaultTablePilotos);

        dcbVehiculoPiloto = new DefaultComboBoxModel();
        comboBoxVehiculoPiloto.setModel(dcbVehiculoPiloto);

        defaultTableCircuitos = new DefaultTableModel();
        tableCircuitos.setModel(defaultTableCircuitos);

        dlmCircuitos = new DefaultListModel<>();

        JlistCiruitos.setModel(dlmCircuitos);

        dlmPilotos = new DefaultListModel<>();
        JlistPilotos.setModel(dlmPilotos);

        dlmPilotosEnCirucito = new DefaultTableModel();
        tablePilotosEncircuito.setModel(dlmPilotosEnCirucito);
        dtmBusquedaPilotos = new DefaultTableModel();
        tableBusquedaPilotos.setModel(dtmBusquedaPilotos);

        dtmBusquedaCircuito = new DefaultTableModel();
        tableBusquedaCircuitos.setModel(dtmBusquedaCircuito);

        dtmBusquedaVehiculo = new DefaultTableModel();
        tableBusquedaVehiculos.setModel(dtmBusquedaVehiculo);

        dlmBusquedaPersonalizada = new DefaultComboBoxModel();
        comboBoxBusquedaPersonalizada.setModel(dlmBusquedaPersonalizada);


        crearMenu();
        frame.pack();


    }

    private void crearMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menuArchivo = new JMenu("Archivo");

        opcionGuardar = new JMenuItem("Guardar");

        opcionSalir = new JMenuItem("salir");
        opcionExportarXML = new JMenuItem("Exportar XML");




        //action comand


        opcionExportarXML.setActionCommand("ExportarXML");



        menuArchivo.add(opcionExportarXML);
        menuArchivo.add(opcionSalir);

        menuBar.add(menuArchivo);

        frame.setJMenuBar(menuBar);

    }

    public void ventanaFaltanDatos(){
        //Creamos un joption pane para encontrar el usuario
        Frame pestaña = new Frame();
        JPanel panel = new JPanel(new BorderLayout());
        JOptionPane.showMessageDialog(pestaña,"No has seleccionado objeto");


    }



}
