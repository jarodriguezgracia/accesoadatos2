package vista;


import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Created by DAM on 25/10/2018.
 */
public class Modelo {

    private Connection conexion;

    public Modelo() {
        /**
        Properties configuracion = new Properties();
        String servidor;
        String puerto;
        String baseDatos;
        String usuario;
        String contrasena;

            try {
                configuracion.load(new FileInputStream("configuracion.props"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            servidor = configuracion.getProperty("servidor");
            puerto = configuracion.getProperty("puerto");
            baseDatos = configuracion.getProperty("basedatos");
            usuario = configuracion.getProperty("usuario");
            contrasena = configuracion.getProperty("contrasena");

        try{
           // conexion= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/final","root","");

            Class.forName("com.mysql.jdbc.Driver").newInstance();

            conexion = DriverManager.getConnection("jdbc:mysql://" + servidor + ":"+puerto+"/"+baseDatos
                    , usuario, contrasena);

            System.out.println("jdbc:mysql://" + servidor + ":"+puerto+"/"+baseDatos
                    + usuario+ contrasena);


        }catch (SQLException e) {
            System.out.println("nO ME LO ESPERABA "+ e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
         **/

    }

    public boolean conectar(){

        Properties configuracion = new Properties();
        String servidor;
        String puerto;
        String baseDatos;
        String usuario;
        String contrasena;
        String protocolo;
        String driver;

        try {
            configuracion.load(new FileInputStream("configuracion.props"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver = configuracion.getProperty("driver");
        protocolo = configuracion.getProperty("protocolo");
        servidor = configuracion.getProperty("servidor");
        puerto = configuracion.getProperty("puerto");
        baseDatos = configuracion.getProperty("basedatos");
        usuario = configuracion.getProperty("usuario");
        contrasena = configuracion.getProperty("contrasena");

        try{
            // conexion= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/final","root","");

            Class.forName(driver).newInstance();

            conexion = DriverManager.getConnection(protocolo + servidor + ":"+puerto+"/"+baseDatos
                    , usuario, contrasena);

            System.out.println(protocolo + servidor + ":"+puerto+"/"+baseDatos
                    + usuario+ contrasena);

        }catch (SQLException e) {
            System.out.println("nO ME LO ESPERABA "+ e);
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Connection crearDBPorProperties(){

        Properties configuracion = new Properties();
        String servidor;
        String puerto;
        String baseDatos;
        String usuario;
        String contrasena;
        String protocolo;
        String driver;

        try {
            configuracion.load(new FileInputStream("configuracion.props"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver = configuracion.getProperty("driver");
        protocolo = configuracion.getProperty("protocolo");
        servidor = configuracion.getProperty("servidor");
        puerto = configuracion.getProperty("puerto");
        baseDatos = configuracion.getProperty("basedatos");
        usuario = configuracion.getProperty("usuario");
        contrasena = configuracion.getProperty("contrasena");

        try{
            // conexion= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/final","root","");

            Class.forName(driver).newInstance();

            conexion = DriverManager.getConnection(protocolo + servidor + ":"+puerto+"/"+baseDatos
                    , usuario, contrasena);

            System.out.println(protocolo + servidor + ":"+puerto+"/"+baseDatos
                    + usuario+ contrasena);

        }catch (SQLException e) {
            System.out.println("nO ME LO ESPERABA "+ e);
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conexion;
    }

    public void insertarVehiculo(String nombre, String precio,String matricula,String tipo,String diseno) throws SQLException {
        String consulta = "INSERT INTO vehiculos(nombre, precio, matricula, tipo, diseno) VALUES(?, ?, ?, ?, ?);";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setDouble(2, Double.parseDouble(precio));
        sentencia.setString(3, matricula);
        sentencia.setString(4, tipo);
        sentencia.setString(5, diseno);


        sentencia.execute();

        if( sentencia == null ){
            sentencia.close();
        }
    }
    public void insertarPilotos(String nombre, int trofeos, LocalDate proximaCarrera,int id) throws SQLException {
        String consulta = "INSERT INTO pilotos(nombre, trofeos, proxima_carrera, vehiculo) VALUES(?, ?, ?, ?)";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setInt(2, trofeos);
        sentencia.setDate(3, Date.valueOf(proximaCarrera));
        sentencia.setInt(4, id);

        sentencia.executeUpdate();

        if( sentencia == null ){
            sentencia.close();
        }
    }

    public void insertarCircuitos(String nombre, String localidad) throws SQLException {
        String consulta = "INSERT INTO circuitos(nombre, localidad) VALUES(?, ?)";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setString(2, localidad);

        sentencia.executeUpdate();

        if( sentencia == null ){
            sentencia.close();
        }
    }

    public void insertarPilotosACircuito(int idCircuito,int idPiloto) throws SQLException {
       // String consulta = "INSERT INTO pilotos_circuito(id_Piloto,id_Circuito) VALUES(?, ?)";

        String consulta = "call alta_Piloto_circuito(?,?)";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, idCircuito);
        sentencia.setInt(2, idPiloto);

        sentencia.executeUpdate();

        if( sentencia == null ){
            sentencia.close();
        }
    }

    public void modificarVehiculo(int idvehiculo,String nombre, Double precio,String matricula,String tipo,String diseno) throws SQLException {
        //String consulta = "INSERT INTO vehiculos(nombre, precio, matricula, tipo, diseno) VALUES(?, ?, ?, ?, ?);";
        String consulta = "UPDATE vehiculos SET nombre="+"\""+nombre+"\"," +" precio="+precio+", matricula="+"\""+matricula+"\","+" tipo="+"\""+tipo+"\","+" diseno="+"\""+diseno
                +"\""+" WHERE vehiculos.id="+idvehiculo+";";


        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.execute();

        if( sentencia == null ){
            sentencia.close();
        }
    }


    public void eliminarVehiculo(String matricula) throws SQLException {
        String consulta = "DELETE FROM vehiculos WHERE matricula = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, matricula);
        sentencia.executeUpdate();

        if( sentencia == null ){
            sentencia.close();
        }
    }

    public void eliminarCircuito(String nombre) throws SQLException {
        String consulta = "call borrar_circuito(?)";
        CallableStatement sentencia = null;

        sentencia = conexion.prepareCall(consulta);
        sentencia.setString(1, nombre);
        sentencia.executeUpdate();

        if( sentencia == null ){
            sentencia.close();
        }
    }

    public void eliminarPiloto(String id) throws SQLException {
        System.out.println(id);
        String consulta = "DELETE FROM pilotos WHERE id = ?";
        String sqlRelacionPiloto = "delete from pilotos_circuito where id_piloto = ?";


        try {
            //Inicia transacción
            conexion.setAutoCommit(false);

            PreparedStatement sentencia =
                    conexion.prepareStatement(consulta,
                            PreparedStatement.RETURN_GENERATED_KEYS);
            sentencia.setString(1, id);
            sentencia.executeUpdate();

            // Obtiene el id del producto que se acaba de registrar
            ResultSet idGenerados = sentencia.getGeneratedKeys();

            sentencia.close();
            PreparedStatement sentenciaRelacionProducto =
                    conexion.prepareStatement(sqlRelacionPiloto);
            sentenciaRelacionProducto.setString(1, id);
            sentenciaRelacionProducto.executeUpdate();

            // Valida la transacción
            conexion.commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {

        }





/**
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, id);
        sentencia.executeUpdate();

        if( sentencia == null ){
            sentencia.close();
        }
 **/
    }

    public ResultSet consultarVehiculos() throws SQLException {
        //String consulta = "SELECT * FROM vehiculos";
        String consulta = "call consultar_Vehiculo();";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();


        return resultado;
    }

    public ResultSet consultarPilotos() throws SQLException {
        String consulta = "SELECT * from pilotos";
        //String consulta = "call consultar_Pilotos();";

       // String consulta = "SELECT pilotos.id,pilotos.nombre,pilotos.trofeos,pilotos.proxima_carrera,pilotos.vehiculo,vehiculos.nombre FROM pilotos,vehiculos where pilotos.vehiculo=vehiculos.id";
       /* String consulta = "SELECT pilotos.id,pilotos.nombre,pilotos.trofeos,\n" +
                "pilotos.proxima_carrera,\n" +
                "vehiculos.nombre FROM pilotos,vehiculos \n" +
                "where vehiculos.id=pilotos.vehiculo;";
                **/
        /**
         * create table pilotos_circuito(
         * id_piloto int not null  references pilotos(id),
         * id_circuito int  not null  references circuitos(id)
         * );
         */
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();


        return resultado;
    }

    public ResultSet consultarCircuitos() throws SQLException {
        //String consulta = "SELECT * FROM circuitos";
        String consulta = "call consultar_circuitos();";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();


        return resultado;
    }


    public void crearDataBase(boolean postgrep){

            String crear= "CREATE DATABASE final;";

            Properties configuracion = new Properties();
            String servidor;
            String puerto;
            String baseDatos;
            String usuario;
            String contrasena;
            String protocolo;
            String driver;

            try {
                configuracion.load(new FileInputStream("configuracion.props"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            driver = configuracion.getProperty("driver");
            protocolo = configuracion.getProperty("protocolo");
            servidor = configuracion.getProperty("servidor");
            puerto = configuracion.getProperty("puerto");
            baseDatos = configuracion.getProperty("basedatos");
            usuario = configuracion.getProperty("usuario");
            contrasena = configuracion.getProperty("contrasena");

            try{
                // conexion= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/final","root","");

                try {
                    Class.forName(driver).newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                System.out.println(protocolo + servidor + ":"+puerto + usuario+ contrasena);
                conexion = DriverManager.getConnection(protocolo + servidor + ":"+puerto+"/", usuario, contrasena);

                System.out.println(protocolo + servidor + ":"+puerto + usuario+ contrasena);

            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }



    public void crearTablaVehiculos(boolean postgrep){
        //aqui me he quedado revisar



/**
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/final","root","");

            //conexion = DriverManager.getConnection(protocolo + servidor + ":"+puerto+"/final", usuario, contrasena);

**/
        conexion=crearDBPorProperties();
        String crear;

        try{
            if (postgrep==true) {
                 crear = "Create table vehiculos(id serial primary key, nombre text, precio REAL,matricula text not null UNIQUE, tipo text,diseno text)";
                System.out.println(crear);
            }else{
                 crear = "Create table vehiculos(id int auto_increment primary key, nombre varchar(50), precio REAL,matricula varchar(50) not null UNIQUE, tipo varchar(40),diseno varchar(40))";
            }
/**qqq
            String crear= "Create table vehiculos(id int auto_increment primary key,\n" +
                    "                    nombre varchar(50),\n" +
                    "                    precio REAL,matricula varchar(50) not null UNIQUE,\n" +
                    "                    tipo varchar(40),diseno varchar(40));";
 **/
            // Create table pilotos(id int auto_increment primary key,
            //            	nombre varchar(50),trofeos int,
            //                proxima_carrera date,
            //                vehiculo int references vehiculos(id)
            //                );

            //Create table circuitos(id int auto_increment primary key,
            //	nombre varchar(50),localidad varchar(50)
            //    );

            //delimiter $$
            //create procedure borrar_jugador(pnombre varchar(40),pid_pabellon int, pid_jugador int, plocalidad_equipo varchar(40))
            //begin
            //if((select count(*) from equipo where id_jugador=pid_jugador))=0 then
            //delete from jugador where id = pid_jugador;
            //else
            //update equipo set id_jugador= null where id_juador = pid_jugador;
            //end if;
            //end;$$
            System.out.println(crear);

            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }
    public void crearTablaPilotos(boolean postgrep){
        //aqui me he quedado revisar
            conexion=crearDBPorProperties();
        String crear;
            try{
                if (postgrep==true){
                    crear = "Create table pilotos(id  serial primary key, nombre text, trofeos integer , proxima_carrera date, vehiculo integer , FOREIGN KEY(vehiculo) references vehiculos(id) ON DELETE set null)";

                }else {

                    crear = "Create table pilotos(id int auto_increment primary key, nombre varchar(50), trofeos int, proxima_carrera date, vehiculo int, FOREIGN KEY(vehiculo) references vehiculos(id) ON DELETE set null)";
                }

            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }
    public void crearTablaCircuitos(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true) {
               crear = "Create table circuitos(id  serial primary key,\n" +
                        "nombre text UNIQUE NOT NULL,\n" +
                        "localidad text\n" +
                        ")";
            }else{
                crear = "Create table circuitos(id int auto_increment primary key,\n" +
                        "nombre varchar(50) UNIQUE NOT NULL,\n" +
                        "localidad varchar(50)\n" +
                        ")";
            }


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }
    public void crearTablaPilotos_Circuitos(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true){
                crear = "create table pilotos_circuito(\n" +
                        "id_piloto integer not null references pilotos(id),\n" +
                        "id_circuito integer  not null  references circuitos(id)\n" +
                        " );";
            }else {
               crear = "create table pilotos_circuito(\n" +
                        "id_piloto int not null references pilotos(id),\n" +
                        "id_circuito int  not null  references circuitos(id)\n" +
                        " );";
            }

            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }

    public void crearFunctionCircuito(boolean postgrep){
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true){
                crear="CREATE FUNCTION idCircuitos(nombreRecivido text) RETURNS integer \n" +
                        "declare respuesta int;\n" +
                        "select id into respuesta from circuitos where circuitos.nombre=nombreRecivido;\n" +
                        "return respuesta;\n" +
                        "+$$ LANGUAGE <SQL>;";
            }else{
                 crear="CREATE FUNCTION idCircuitos(nombreRecivido varchar(100)) RETURNS int \n" +
                        "BEGIN\n" +
                        "declare respuesta int;\n" +
                        "select id into respuesta from circuitos where circuitos.nombre=nombreRecivido;\n" +
                        "return respuesta;\n" +
                        "end; \n";
            }

            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }
        public void crearProcedureConVehiculo(boolean postgrep){
        //aqui me he quedado revisar
            conexion=crearDBPorProperties();
            String crear;
        try{
            if(postgrep==true){
                crear = "create procedure consultar_Vehiculo()\n" +
                        "begin\n" +
                        "select * \n" +
                        "from vehiculos;\n" +
                        "end;";

            }else {
                crear = "create procedure consultar_Vehiculo()\n" +
                        "begin\n" +
                        "select * \n" +
                        "from vehiculos;\n" +
                        "end;";
            }
            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }
    public void crearProcedureConCircuitos(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true){
                crear = "create procedure consultar_Circuitos()\n" +
                        "begin\n" +
                        "select * \n" +
                        "from circuitos;\n" +
                        "end;";
            }else {
                 crear = "create procedure consultar_Circuitos()\n" +
                        "begin\n" +
                        "select * \n" +
                        "from circuitos;\n" +
                        "end;";
            }
            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }

    public void crearProcedureAltaPilotoCircuito(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true){
                crear = "create procedure alta_Piloto_circuito(id_circuito integer ,id_piloto integer )\n" +
                        "begin\n" +
                        "if((select count(*) from pilotos_circuito where pilotos_circuito.id_circuito=id_circuito and pilotos_circuito.id_piloto=id_piloto))>0 \n" +
                        "then\n" +
                        "select \"Piloto ya añadido en circuito\";\n" +
                        "else\n" +
                        "insert into pilotos_circuito(id_piloto,id_circuito) \n" +
                        "values (id_piloto,id_circuito);\n" +
                        "end if;\n" +
                        "end;";
            }else {
                 crear = "create procedure alta_Piloto_circuito(id_circuito int,id_piloto int)\n" +
                        "begin\n" +
                        "if((select count(*) from pilotos_circuito where pilotos_circuito.id_circuito=id_circuito and pilotos_circuito.id_piloto=id_piloto))>0 \n" +
                        "then\n" +
                        "select \"Piloto ya añadido en circuito\";\n" +
                        "else\n" +
                        "insert into pilotos_circuito(id_piloto,id_circuito) \n" +
                        "values (id_piloto,id_circuito);\n" +
                        "end if;\n" +
                        "end;";
            }
            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }

    public void crearConsultarPilotos(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true){
                crear = "create function consultar_pilotos(pid_circuito integer )\n" +
                        "returns integer \n" +
                        "begin\n" +
                        "return(select count(*) from pilotos_circuito where id_circuito=pid_circuito);\n" +
                        "end;" + " $$ LANGUAGE sql;";
            }else {
                crear = "create function consultar_pilotos(pid_circuito int)\n" +
                        "returns int\n" +
                        "begin\n" +
                        "return(select count(*) from pilotos_circuito where id_circuito=pid_circuito);\n" +
                        "end;";
            }
            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }

    public void crearConsultarCircuitos(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;
        try{
            if(postgrep==true){
                crear = "create function consultar_circuitos(pid_piloto integer )\n" +
                        "returns integer \n" +
                        "begin\n" +
                        "return(select count(*) from pilotos_circuito where id_piloto=pid_piloto);\n" +
                        "end;" + "$$ LANGUAGE sql; ";
            }else {
                 crear = "create function consultar_circuitos(pid_piloto int)\n" +
                        "returns int\n" +
                        "begin\n" +
                        "return(select count(*) from pilotos_circuito where id_piloto=pid_piloto);\n" +
                        "end;";
            }
            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }

    public void crearProcedureBorraCircuito(boolean postgrep){
        //aqui me he quedado revisar
        conexion=crearDBPorProperties();
        String crear;

        try{
            if(postgrep==true){
               crear="create procedure borrar_circuito(pnombre text))\n" +
                        "begin\n" +
                        "declare pid integer ;\n" +
                        "if((select count(*) from circuitos where nombre like pnombre))=0 then\n" +
                        "delete from circuitos where nombre like pnombre;\n" +
                        "else\n" +
                        "set pid = (select id from circuitos where nombre like pnombre);\n" +
                        "delete from pilotos_circuito where id_circuito= pid;\n" +
                        "delete from circuitos where id=pid;\n" +
                        "end if;\n" +
                        "end;";
            }else{
                crear="create procedure borrar_circuito(pnombre varchar(50))\n" +
                        "begin\n" +
                        "declare pid int(11);\n" +
                        "if((select count(*) from circuitos where nombre like pnombre))=0 then\n" +
                        "delete from circuitos where nombre like pnombre;\n" +
                        "else\n" +
                        "set pid = (select id from circuitos where nombre like pnombre);\n" +
                        "delete from pilotos_circuito where id_circuito= pid;\n" +
                        "delete from circuitos where id=pid;\n" +
                        "end if;\n" +
                        "end;";
            }

            System.out.println(crear);


            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(crear);

        }catch (SQLException e) {
            System.out.println("aqui llego");
            System.out.println(e);
        }
        //cerrar la conexion
        try{
            conexion.close();
        }catch (SQLException e) {
            System.out.println("No se puede cerra "+e);
        }
    }

    public ResultSet consultarPilotosEnCircuito() {
        String consulta = "SELECT circuitos.nombre,circuitos.localidad,pilotos.nombre,pilotos.trofeos FROM pilotos,circuitos,pilotos_circuito\n" +
                "where pilotos_circuito.id_circuito=circuitos.id and pilotos_circuito.id_piloto=pilotos.id";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return resultado;
    }
//finalizar esto
    public int consultarIdCircuito(String s) throws SQLException {
        String consulta = "select id from circuitos where nombre="+"\""+s+"\""+";";


        PreparedStatement sentencia = null;
        ResultSet resultado = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ResultSetMetaData rsMd = resultado.getMetaData();
        //La cantidad de columnas que tiene la consulta

        int cantidadColumnas = rsMd.getColumnCount();
        //Establecer como cabezeras el nombre de las colimnas

        for (int i = 1; i <= cantidadColumnas; i++) {

        }
        //Creando las filas para el JTable
        Object[] fila = null;
        while (resultado.next()) {
            fila = new Object[cantidadColumnas];
            for (int i = 0; i < cantidadColumnas; i++) {

                fila[i]=resultado.getObject(i+1);
            }

        }
        resultado.close();

        int result = Integer.parseInt(String.valueOf(fila[0]));





        return result;
    }

    public ResultSet buscarPorNombre(String cad) {
        cad=cad+'%';
       // String consulta = "SELECT * FROM final.circuitos,final.vehiculos,final.pilotos\n" + "where circuitos.nombre like 'k%' or vehiculos.nombre like 'k%' or pilotos.nombre like 'k%';";
        /**
        String consulta="select *\n" +
                "from circuitos\n" +
                "where circuitos.nombre like ?";
         **/
        /**
        String consulta="select *\n" +
                "from circuitos,pilotos,vehiculos\n" +
                "where circuitos.nombre like ? or pilotos.nombre like ? or vehiculos.nombre like ?";
        **/

        String consulta="select *\n" +
                "from circuitos,pilotos,vehiculos\n" +
                "where circuitos.nombre like ? or pilotos.nombre like ? or vehiculos.nombre like ? and pilotos.vehiculo=vehiculos.id";
      /**
        String consulta="select circuitos.nombre,pilotos.nombre,vehiculos.nombre\n" +
                "from circuitos,pilotos,vehiculos,pilotos_circuito\n" +
                "where circuitos.nombre like ? or pilotos.nombre like ? or vehiculos.nombre like ? and pilotos.vehiculo=vehiculos.id \n" +
                "and pilotos_circuito.id_piloto=pilotos.id";
       **/
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        System.out.println(consulta);
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, cad);
            sentencia.setString(2, cad);
            sentencia.setString(3, cad);

            System.out.println(sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return resultado;
    }

    public ResultSet buscarPorNombre(String cad,String tipo) {
        cad=cad+'%';
        String consulta = "";
        if(tipo.equals("circuitos")) {
            consulta = "select *\n" +
                    "from circuitos\n" +
                    "where circuitos.nombre like ?";
        }else if(tipo.equals("vehiculo")) {
            consulta = "select *\n" +
                    "from vehiculos\n" +
                    "where vehiculos.nombre like ?";
        }else if(tipo.equals("piloto")) {
            consulta = "select *\n" +
                    "from pilotos\n" +
                    "where pilotos.nombre like ?";
        }
        /**
        String consulta="select *\n" +
                "from circuitos,pilotos,vehiculos\n" +
                "where circuitos.nombre like ? or pilotos.nombre like ? or pilotos.nombre like ?";
        **/
         PreparedStatement sentencia = null;
        ResultSet resultado = null;
        System.out.println(consulta);
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, cad);

            System.out.println(sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return resultado;
    }



    public ResultSet buscarPersonalizado(String cad,String tipo,String campo) {
        cad=cad+'%';
        String consulta = "";
        if(tipo.equals("circuitos")) {
            consulta = "select *\n" +
                    "from circuitos\n" +
                    "where circuitos."+campo+" like ?";
        }else if(tipo.equals("vehiculo")) {
            consulta = "select *\n" +
                    "from vehiculos\n" +
                    "where vehiculos."+campo+" like ?";
        }else if(tipo.equals("piloto")) {
            consulta = "select *\n" +
                    "from pilotos\n" +
                    "where pilotos."+campo+" like ?";
        }
        /**
         String consulta="select *\n" +
         "from circuitos,pilotos,vehiculos\n" +
         "where circuitos.nombre like ? or pilotos.nombre like ? or pilotos.nombre like ?";
         **/
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        System.out.println(consulta);
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, cad);

            System.out.println(sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return resultado;
    }

    public void modificarPilotos(int idPilotos, String nombre, int trofeos, LocalDate proxima_carrera, int idVehiculoPiloto) {
        //String consulta = "INSERT INTO vehiculos(nombre, precio, matricula, tipo, diseno) VALUES(?, ?, ?, ?, ?);";
        String consulta = "UPDATE pilotos SET nombre="+"\""+nombre+"\"," +" trofeos="+trofeos+", proxima_carrera="+"\""+proxima_carrera+"\","+" vehiculo="+idVehiculoPiloto+" WHERE pilotos.id="+idPilotos+";";
        System.out.println(consulta);
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if( sentencia == null ){
            try {
                sentencia.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void modificarCircuitos(int idCircuito, String nombre, String localidad) {
        String consulta = "UPDATE circuitos SET nombre="+"\""+nombre+"\"," +" localidad="+"\""+localidad+"\""+" WHERE circuitos.id="+idCircuito+";";
        PreparedStatement sentencia = null;
        try {

            sentencia = conexion.prepareStatement(consulta);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if( sentencia == null ){
            try {
                sentencia.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ResultSet consultarPilotosdecircuito(int id) {
        String consulta = "select consultar_pilotos(?)";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return resultado;


    }

    public ResultSet consultarCircuitosDePiloto(int id) {
        String consulta = "select consultar_circuitos(?)";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;


            try {
                sentencia = conexion.prepareStatement(consulta);
                sentencia.setInt(1, id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                resultado = sentencia.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }


            return resultado;

        }
    }


