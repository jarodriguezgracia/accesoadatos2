package vista;


//Configurar los radio button para que cuando se seleccionen aparezcan los coches o las motos


import com.sun.deploy.config.ClientConfig;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import usuarios.ListaUsuarios;
import usuarios.Usuario;


import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Vector;

/**
 * Created by DAM on 23/10/2018.
 */
//implemeno los ActionListener ListSelectionListener WindowListener
public class Controlador implements ActionListener,ListSelectionListener,MouseListener,KeyListener {
    private Vista vista;
    private Modelo modelo;
    private File ficheroDatos;
    private ListaUsuarios listaUser;
    HiloUpdate hilo;

    /**
     * @param vista
     * @param circuito
     * @apiNote recivimos un base y una vista para inicializar el programa
     * ademas de a añadir los diferentes listener a los objetos y inicalizamos la lista
     */
    public Controlador(Vista vista, Modelo circuito) {

        this.vista = vista;
        this.modelo = circuito;
        listaUser = new ListaUsuarios();



        //actualizo
        boolean respuesta;
        respuesta=modelo.conectar();
        if(respuesta==true && vista.postGrepRadioButton.isSelected()==false) {

            actualizarDatos();
            /**
            actualizar(vista.defaultTablePilotos, "pilotos");
            actualizar(vista.defaultTableVehiculo, "vehiculos");
            actualizar(vista.dlmPilotosEnCirucito, "pilotosEnCircuitos");
            actualizar(vista.defaultTableCircuitos, "circuitos");
            actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
            actualizarJlistCircuitos(vista.dlmPilotos, "pilotos");
            actualizarComboxVehiculo();

            HiloUpdate hilo= new HiloUpdate(vista,modelo,this);

            hilo.start();
             **/
        }else{
            System.out.println("llego");

            JOptionPane.showMessageDialog(null, "No hay DB disponible implemente 1 desde admin",
                    "WARNING_MESSAGE", JOptionPane.ERROR_MESSAGE);
        }

        vista.dlmBusquedaPersonalizada.addElement("circuitos");
        vista.dlmBusquedaPersonalizada.addElement("pilotos");
        vista.dlmBusquedaPersonalizada.addElement("vehiculos");

        vista.añadirPilotosHaCircuitoButton.setEnabled(false);



        //configurar la visibilida
        vista.tabbedPane1.setEnabledAt(3, false);
        vista.panelNewUser.setVisible(false);


        //metodos para añadir los actionListener
        añadirActionListener(this);

        //añador los list selecion listener
        añadirListSelectionlistener(this);

        añadirMouseListener(this);

        añadirKeyListener();



    }

    private void añadirKeyListener() {
    vista.textBusquedaGeneral.addKeyListener(this);
    vista.txtTextoBusquedaPerosnalizada.addKeyListener(this);
    }

    private void añadirMouseListener(Controlador controlador) {
        vista.tableVehiculo.addMouseListener(controlador);
        vista.tablePilotos.addMouseListener(controlador);
        vista.tableCircuitos.addMouseListener(controlador);
    }

    /**
     * Metodo que añade el listSelection listener a la lista
     *
     * @param listener
     */
    private void añadirListSelectionlistener(Controlador listener) {
        vista.JlistPilotos.addListSelectionListener(listener);
        vista.JlistCiruitos.addListSelectionListener(listener);
    }

    /**
     * metodos para añadir el actionListener
     *
     * @param listener
     */
    private void añadirActionListener(Controlador listener) {

        //implemento los listener a los botones
        this.vista.ButtonAltaPiloto.addActionListener(listener);
        this.vista.ButtonEliminarPiloto.addActionListener(listener);
        vista.btAltaCircuito.addActionListener(listener);
        vista.btnEliminarCircuito.addActionListener(listener);
        vista.añadirPilotosHaCircuitoButton.addActionListener(listener);

        this.vista.ButtonAltaVehiculo.addActionListener(listener);
        this.vista.ButtonEliminarVehiculo.addActionListener(listener);
        vista.comboBoxVehiculoPiloto.addActionListener(listener);

        vista.btModificarCircuito.addActionListener(listener);
        vista.buttonModificarPiloto.addActionListener(listener);
        vista.ButtonModificarVehiculo.addActionListener(listener);
        vista.accederButton.addActionListener(listener);
        vista.añadiruser.addActionListener(listener);
        vista.btnCambiarPreferencias.addActionListener(listener);

        vista.ConsultarCir.addActionListener(this);
        vista.consultarCircuitosButton.addActionListener(this);

        vista.crearPredeterminadaFinalButton.addActionListener(this);
        vista.opcionExportarXML.addActionListener(this);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {

            case "ExportarXML":

                System.out.println(vista.tableCircuitos.getColumnCount());
                System.out.println(vista.tableCircuitos.getRowCount());

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = null;
                try {
                    builder = factory.newDocumentBuilder();
                } catch (ParserConfigurationException e1) {
                    e1.printStackTrace();
                }
                DOMImplementation dom = builder.getDOMImplementation();

                Document documento = dom.createDocument(null, "xml", null);

                //Creo el primer nodo del fichero XML
                //Corresponde a la etiqueta "libreria"
                org.w3c.dom.Element raiz = documento.createElement("Aplicacion");
                documento.getDocumentElement().appendChild(raiz);

                org.w3c.dom.Element nodoCoche = null;
                Element nodoDatos = null;
                Text dato = null;

                int numeroColumnas = 0;
                nodoCoche = documento.createElement("Circuitos");
                raiz.appendChild(nodoCoche);
                while (numeroColumnas!=vista.tableCircuitos.getRowCount()){
                    System.out.println("valor de numColum "+ numeroColumnas);
                    nodoCoche = documento.createElement("Circuito");
                    raiz.appendChild(nodoCoche);
                    for (int u=0;u<vista.tableCircuitos.getColumnCount();u++){
                        if(u==0){
                            nodoDatos = documento.createElement("id");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableCircuitos.getValueAt(numeroColumnas,u)));
                            nodoDatos.appendChild(dato);
                        }else if(u==1){
                            nodoDatos = documento.createElement("nombre");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableCircuitos.getValueAt(numeroColumnas,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==2){
                            nodoDatos = documento.createElement("localidad");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableCircuitos.getValueAt(numeroColumnas,u)));
                            nodoDatos.appendChild(dato);
                        }
                    }
                    numeroColumnas++;

                }
                int numeroColumnasPiloto = 0;
                nodoCoche = documento.createElement("pilotos");
                raiz.appendChild(nodoCoche);
                while (numeroColumnasPiloto!=vista.tablePilotos.getRowCount()){
                    nodoCoche = documento.createElement("Piloto");
                    raiz.appendChild(nodoCoche);
                    for (int u=0;u<vista.tablePilotos.getColumnCount();u++){
                        if(u==0){
                            nodoDatos = documento.createElement("id");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tablePilotos.getValueAt(numeroColumnasPiloto,u)));
                            nodoDatos.appendChild(dato);
                        }else if(u==1){
                            nodoDatos = documento.createElement("nombre");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tablePilotos.getValueAt(numeroColumnasPiloto,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==2){
                            nodoDatos = documento.createElement("trofeos");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tablePilotos.getValueAt(numeroColumnasPiloto,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==3){
                            nodoDatos = documento.createElement("Proxima_carrera");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tablePilotos.getValueAt(numeroColumnasPiloto,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==4){
                            nodoDatos = documento.createElement("vehiculo");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tablePilotos.getValueAt(numeroColumnasPiloto,u)));
                            nodoDatos.appendChild(dato);
                        }
                    }
                    numeroColumnasPiloto++;

                }
                int numeroColumnasVehiculo = 0;
                nodoCoche = documento.createElement("Vehiculos");
                raiz.appendChild(nodoCoche);
                while (numeroColumnasVehiculo!=vista.tableVehiculo.getRowCount()){
                    nodoCoche = documento.createElement("vehiculo");
                    raiz.appendChild(nodoCoche);
                    for (int u=0;u<vista.tableVehiculo.getColumnCount();u++){
                        if(u==0){
                            nodoDatos = documento.createElement("id");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableVehiculo.getValueAt(numeroColumnasVehiculo,u)));
                            nodoDatos.appendChild(dato);
                        }else if(u==1){
                            nodoDatos = documento.createElement("nombre");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableVehiculo.getValueAt(numeroColumnasVehiculo,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==2){
                            nodoDatos = documento.createElement("precio");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableVehiculo.getValueAt(numeroColumnasVehiculo,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==3){
                            nodoDatos = documento.createElement("matricula");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableVehiculo.getValueAt(numeroColumnasVehiculo,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==4){
                            nodoDatos = documento.createElement("tipo");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableVehiculo.getValueAt(numeroColumnasVehiculo,u)));
                            nodoDatos.appendChild(dato);
                        }else if (u==5){
                            nodoDatos = documento.createElement("diseno");
                            nodoCoche.appendChild(nodoDatos);
                            dato = documento.createTextNode(String.valueOf(vista.tableVehiculo.getValueAt(numeroColumnasVehiculo,u)));
                            nodoDatos.appendChild(dato);
                        }
                    }
                    numeroColumnasVehiculo++;

                }
                Source src = new DOMSource(documento);
                Result resultado = new StreamResult("backup.mxl");

                Transformer transformer = null;
                try {
                    transformer = TransformerFactory.newInstance().newTransformer();
                } catch (TransformerConfigurationException e1) {
                    e1.printStackTrace();
                }
                try {
                    transformer.transform(src, resultado);
                } catch (TransformerException e1) {
                    e1.printStackTrace();
                }

                break;

            case "createPredeterminada":
                boolean postgrep = vista.postGrepRadioButton.isSelected();
                if(postgrep==true){

                    modelo.crearDataBase(postgrep);
                    modelo.crearTablaVehiculos(postgrep);
                    modelo.crearTablaPilotos(postgrep);
                    modelo.crearTablaCircuitos(postgrep);
                    modelo.crearTablaPilotos_Circuitos(postgrep);
                }else{
                    modelo.crearDataBase(postgrep);
                    modelo.crearTablaVehiculos(postgrep);
                    modelo.crearTablaPilotos(postgrep);
                    modelo.crearTablaCircuitos(postgrep);
                    modelo.crearTablaPilotos_Circuitos(postgrep);
                    modelo.crearFunctionCircuito(postgrep);
                    modelo.crearProcedureConVehiculo(postgrep);
                    modelo.crearProcedureConCircuitos(postgrep);
                    modelo.crearProcedureAltaPilotoCircuito(postgrep);
                    modelo.crearConsultarPilotos(postgrep);
                    modelo.crearConsultarCircuitos(postgrep);
                    modelo.crearProcedureBorraCircuito(postgrep);
                }

                boolean conectar;
                conectar=modelo.conectar();
        if(vista.postGrepRadioButton.isSelected()==true){

        }else {
            if (conectar == true) {
                actualizarDatos();
            }
        }
                break;

            case "consultar":
                ResultSet result = null;

                    if (e.getSource() == vista.ConsultarCir) {
                        int id = (int) Integer.parseInt(String.valueOf(vista.defaultTableCircuitos.getValueAt(vista.tableCircuitos.getSelectedRow(), 0)));
                        System.out.println(id);
                        //extraer los id y colocar la ventana de colocacion
                        result=modelo.consultarPilotosdecircuito(id);

                        try {
                            result.next();
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        int numero = 0;
                        try {
                            numero= (int) result.getObject(1);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        String cad = "el circuito tiene "+numero+" pilotos";
                        JOptionPane.showMessageDialog(null,cad);
                    } else if (e.getSource() == vista.consultarCircuitosButton) {
                        int id = (int) Integer.parseInt(String.valueOf(vista.defaultTablePilotos.getValueAt(vista.tablePilotos.getSelectedRow(), 0)));
                        System.out.println(id);
                        result=modelo.consultarCircuitosDePiloto(id);

                        try {
                            result.next();
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        int numero = 0;
                        try {
                            numero= (int) result.getObject(1);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        String cad = "el piloto esta en "+numero+" circuitos";
                        JOptionPane.showMessageDialog(null,cad);
                    }

                break;
            case "AltaVehiculo":
                //(String nombre, float precio,String matricula,String tipo,String diseño)

                try {
                    modelo.insertarVehiculo(vista.textNombreVehiculo.getText(), vista.textPrecioVehiculo.getText(),
                            vista.txtMatriculaVehiculo.getText(), vista.textTipoVehiculo.getText(), vista.textDiseñoVehiculo.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                actualizar(vista.defaultTableVehiculo, "vehiculos");

                actualizarComboxVehiculo();

                break;
            case "altaCircuito":

                try {
                    modelo.insertarCircuitos(vista.txtNombreCircuito.getText(), vista.textLocalidadCircuito.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                actualizar(vista.defaultTableCircuitos, "circuitos");

                actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
                break;
            case "AltaPiloto":
                try {
                    String cad = vista.comboBoxVehiculoPiloto.getSelectedItem().toString();
                    String separacion[] = cad.split(" ");

                    int id = Integer.parseInt(separacion[0]);
                    modelo.insertarPilotos(vista.textNombrePiloto.getText(), Integer.parseInt(vista.textTrofeosPiloto.getText()),
                            vista.txtFechaPiloto.getDate(), id);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                actualizar(vista.defaultTablePilotos, "pilotos");
                actualizarJlistCircuitos(vista.dlmPilotos, "pilotos");


                break;
            case "eliminar":
                if (e.getSource() == vista.ButtonEliminarVehiculo) {

                    try {
                        modelo.eliminarVehiculo(vista.txtMatriculaVehiculo.getText());
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                } else if (e.getSource() == vista.ButtonEliminarPiloto) {
                    try {
                        modelo.eliminarPiloto(vista.txtIdPiloto.getText());

                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                } else if (e.getSource() == vista.btnEliminarCircuito) {
                    try {

                        modelo.eliminarCircuito(vista.txtNombreCircuito.getText());

                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                actualizar(vista.defaultTableCircuitos, "circuitos");
                actualizar(vista.defaultTablePilotos, "pilotos");
                actualizar(vista.defaultTableVehiculo, "vehiculos");

                actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
                actualizarJlistCircuitos(vista.dlmPilotos, "pilotos");
                actualizarComboxVehiculo();
                actualizar(vista.dlmPilotosEnCirucito, "pilotosEnCircuitos");

                break;

            case "añadirPilotosCircuito":
                String cadena = (String) vista.JlistPilotos.getSelectedValue();
                String caPiloto[] = cadena.split(" ");


                int idPiloto = Integer.parseInt(caPiloto[1]);

                String cadenaCircuito = vista.JlistCiruitos.getSelectedValue().toString();

                String[] caCircuito = cadenaCircuito.split("\"");
                for (int i = 0; i < caCircuito.length; i++) {
                    System.out.println(caCircuito[i]);
                }


                try {
                    int idCircuito = modelo.consultarIdCircuito(caCircuito[1]);
                    modelo.insertarPilotosACircuito(idCircuito, idPiloto);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                actualizar(vista.dlmPilotosEnCirucito, "pilotosEnCircuitos");


                break;

            case "modificar":
                if (e.getSource() == vista.ButtonModificarVehiculo) {

                    Vector datos = vista.defaultTableVehiculo.getDataVector();
                    try {
                        String cad = String.valueOf(datos.get(vista.tableVehiculo.getSelectedRow()));
                        String[] sepacion = cad.split("\\[");
                        String[] sepacionFinal = sepacion[1].split(",");

                        int idVehiculo = Integer.parseInt(sepacionFinal[0]);


                    try {
                        modelo.modificarVehiculo(idVehiculo, vista.textNombreVehiculo.getText(), Double.valueOf(vista.textPrecioVehiculo.getText()),
                                vista.txtMatriculaVehiculo.getText(), vista.textTipoVehiculo.getText(), vista.textDiseñoVehiculo.getText());
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    }catch (ArrayIndexOutOfBoundsException err){
                        vista.ventanaFaltanDatos();
                    }

                } else if (e.getSource() == vista.buttonModificarPiloto) {
                    Vector datos = vista.defaultTablePilotos.getDataVector();
                    String cad = null;
                    try {
                        cad = String.valueOf(datos.get(vista.tablePilotos.getSelectedRow()));

                    String[] sepacion = cad.split("\\[");
                    String[] sepacionFinal = sepacion[1].split(",");
                    int idPilotos = Integer.parseInt(sepacionFinal[0]);


                    String cadenaVehiculo = vista.comboBoxVehiculoPiloto.getSelectedItem().toString();

                    String[] cadenavehiculobuena = cadenaVehiculo.split(" ");

                    int idVehiculoPiloto = Integer.parseInt(cadenavehiculobuena[0]);


                    //falta el vehiculo
                    modelo.modificarPilotos(idPilotos, vista.textNombrePiloto.getText(), Integer.parseInt(vista.textTrofeosPiloto.getText()),
                            vista.txtFechaPiloto.getDate(), idVehiculoPiloto);
                    }catch (ArrayIndexOutOfBoundsException ee){
                        vista.ventanaFaltanDatos();
                    }


                } else if (e.getSource() == vista.btModificarCircuito) {
                    Vector datos = vista.defaultTableCircuitos.getDataVector();
                    try {
                        String cad = String.valueOf(datos.get(vista.tableCircuitos.getSelectedRow()));
                        String[] sepacion = cad.split("\\[");
                        String[] sepacionFinal = sepacion[1].split(",");


                        int idCircuito = Integer.parseInt(sepacionFinal[0]);

                        modelo.modificarCircuitos(idCircuito, vista.txtNombreCircuito.getText(), vista.textLocalidadCircuito.getText());
                    }catch (ArrayIndexOutOfBoundsException asd){
                        vista.ventanaFaltanDatos();
                    }
                }

                actualizar(vista.defaultTablePilotos, "pilotos");
                actualizar(vista.defaultTableVehiculo, "vehiculos");
                actualizar(vista.dlmPilotosEnCirucito, "pilotosEnCircuitos");
                actualizar(vista.defaultTableCircuitos, "circuitos");
                actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
                actualizarJlistCircuitos(vista.dlmPilotos, "pilotos");
                actualizarComboxVehiculo();

                break;
            case "Acceder":
                String nombre = vista.textUser.getText();
                String contrasena = vista.textContraseña.getText();


                for (int i = 0; i < listaUser.getListaUsuario().size(); i++) {
                    if (nombre.equals(listaUser.getListaUsuario().get(i).getNombre())) {
                        if (contrasena.equals(listaUser.getListaUsuario().get(i).getContrasena())) {
                            if (listaUser.getListaUsuario().get(i).isAdministrador() == true) {
                                vista.tabbedPane1.setEnabledAt(3, true);
                                vista.panelNewUser.setVisible(true);

                            } else {
                                vista.tabbedPane1.setEnabledAt(3, false);
                                vista.panelNewUser.setVisible(false);
                            }
                        }
                    }
                }

                break;

            case "añadirUser":
                String user = vista.textaddUser.getText();
                String password = vista.textaddPassword.getText();

                Usuario user1 = new Usuario(user, password, false);
                listaUser.getListaUsuario().add(user1);

                break;

            case "cambiarPreferencias":
                Properties configuracion = new Properties();
                if (vista.mySQLRadioButton.isSelected()) {
                    configuracion.setProperty("driver", "com.mysql.jdbc.Driver");
                    configuracion.setProperty("protocolo", "jdbc:mysql://");
                } else {
                    hilo.cambiarcontador();
                    configuracion.setProperty("driver", "org.postgresql.Driver");
                    configuracion.setProperty("protocolo", "jdbc:postgresql://");
                }

                configuracion.setProperty("servidor", vista.textServer.getText());
                configuracion.setProperty("basedatos", vista.textDB.getText());
                configuracion.setProperty("puerto", vista.textPuerto.getText());
                configuracion.setProperty("usuario", vista.textUserDB.getText());
                configuracion.setProperty("contrasena", vista.textPasswordDB.getText());

                try {
                    configuracion.store(new FileOutputStream("configuracion.props"), "");
                } catch (FileNotFoundException fnfe) {
                    // TODO Mostrar mensaje de error
                } catch (IOException ioe) {
                    // TODO Mostrar mensaje de error
                }
                boolean conectar1;
                conectar1=modelo.conectar();
                if(vista.postGrepRadioButton.isSelected()==true){

                }else {
                    if (conectar1 == true) {
                        actualizarDatos();
                    }
                }
                break;
        }

    }

    private void actualizarDatos() {
        actualizar(vista.defaultTablePilotos, "pilotos");
        actualizar(vista.defaultTableVehiculo, "vehiculos");
        actualizar(vista.dlmPilotosEnCirucito, "pilotosEnCircuitos");
        actualizar(vista.defaultTableCircuitos, "circuitos");
        actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
        actualizarJlistCircuitos(vista.dlmPilotos, "pilotos");
        actualizarComboxVehiculo();

        hilo= new HiloUpdate(vista,modelo,this);

        hilo.start();

    }

    public void actualizar(DefaultTableModel dlm, String tipo) {
        dlm.setRowCount(0);
        dlm.setColumnCount(0);
        ResultSet rs = null;
        try {
            if (tipo.equals("vehiculos")) {
                rs = modelo.consultarVehiculos();
            } else if (tipo.equals("pilotos")) {
                rs = modelo.consultarPilotos();
            } else if (tipo.equals("circuitos")) {
                rs = modelo.consultarCircuitos();
            } else if (tipo.equals("pilotosEnCircuitos")) {
                rs = modelo.consultarPilotosEnCircuito();
            }
            //Obteniendo la informacion de las columnas que estan siendo consultadas

            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la consulta

            int cantidadColumnas = rsMd.getColumnCount();
            //Establecer como cabezeras el nombre de las colimnas

            for (int i = 1; i <= cantidadColumnas; i++) {
                dlm.addColumn(rsMd.getColumnLabel(i));
            }
            //Creando las filas para el JTable
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs.getObject(i + 1);

                }
                for (int i = 0; i <fila.length ; i++) {

                    //System.out.println("lalalal" +fila[i]);
                    if(fila[i]== null){
                        fila[i]="no tiene";
                    }
                }

                    dlm.addRow(fila);

            }
            rs.close();

        } catch (Exception ex) {
            System.out.println("aqui llego");
            ex.printStackTrace();
        }
    }

    private void actualizarComboxVehiculo() {
        vista.dcbVehiculoPiloto.removeAllElements();
        ArrayList<Object> datos = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = modelo.consultarVehiculos();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSetMetaData rsMd = null;
        try {
            rsMd = rs.getMetaData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //La cantidad de columnas que tiene la consulta

        int cantidadColumnas = 0;
        try {
            cantidadColumnas = rsMd.getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Establecer como cabezeras el nombre de las colimnas

        //Creando las filas para el JTable
        while (true) {
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }

            datos.clear();
            for (int i = 0; i < cantidadColumnas; i++) {

                try {
                    datos.add(rs.getObject(i + 1));
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
            String cadena = datos.get(0) + " Nombre: " + datos.get(1) + " Matricula: " + datos.get(3);
            vista.dcbVehiculoPiloto.addElement(cadena);
        }
        try {
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void actualizarJlistCircuitos(DefaultListModel dlm, String tipo) {
        dlm.clear();
        ArrayList<Object> datos = new ArrayList<>();
        ResultSet rs = null;
        try {
            if (tipo.equals("circuitos")) {
                rs = modelo.consultarCircuitos();
            } else if (tipo.equals("pilotos")) {
                rs = modelo.consultarPilotos();

            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("aqui llego");
        }
        ResultSetMetaData rsMd = null;
        try {
            rsMd = rs.getMetaData();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("aqui llego");
        }
        //La cantidad de columnas que tiene la consulta

        int cantidadColumnas = 0;
        try {
            cantidadColumnas = rsMd.getColumnCount();
        } catch (SQLException e) {
            System.out.println("aqui llego");
            e.printStackTrace();
        }
        //Establecer como cabezeras el nombre de las colimnas

        //Creando las filas para el JTable
        while (true) {
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            datos.clear();
            for (int i = 0; i < cantidadColumnas; i++) {

                try {
                    if (rs.getObject(i + 1) != null) {
                        datos.add(rs.getObject(i + 1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
            String cadena = "";
            if (tipo.equals("circuitos")) {
                cadena = "Nombre: " + "\"" + datos.get(1).toString() + "\"" + " Localidad: " + datos.get(2).toString();
            } else if (tipo.equals("pilotos")) {
                cadena = "id: " + datos.get(0).toString() + " Nombre: " + datos.get(1).toString();
            }

            dlm.addElement(cadena);
        }
        try {
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (vista.JlistPilotos.getSelectedValue() != null && vista.JlistCiruitos.getSelectedValue() != null) {
            vista.añadirPilotosHaCircuitoButton.setEnabled(true);
        } else {
            vista.añadirPilotosHaCircuitoButton.setEnabled(false);

        }


    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getSource() == vista.tableVehiculo) {
            colocarDatosVehiculoEnTextField();

        }
        if (e.getSource() == vista.tablePilotos) {
            colocarDatosPilotosEnTextField();

        }

        if (e.getSource() == vista.tableCircuitos) {
            colocarDatosCircuitoEnTextField();
        }

    }

    private void colocarDatosCircuitoEnTextField() {
        int filaseleccionada;

        //Guardamos en un entero la fila seleccionada.
        filaseleccionada = vista.tableCircuitos.getSelectedRow();
        if (filaseleccionada == -1) {
            JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna fila.");
        } else {
            //String ayuda = tabla.getValueAt(filaseleccionada, num_columna).toString());
            String Nombre = String.valueOf(vista.tableCircuitos.getValueAt(filaseleccionada, 1));
            String localidad = String.valueOf(vista.tableCircuitos.getValueAt(filaseleccionada, 2));


            vista.txtNombreCircuito.setText(Nombre);
            vista.textLocalidadCircuito.setText(localidad);

        }
    }

    private void colocarDatosPilotosEnTextField() {
        int filaseleccionada;

        //Guardamos en un entero la fila seleccionada.
        filaseleccionada = vista.tablePilotos.getSelectedRow();
        if (filaseleccionada == -1) {
            JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna fila.");
        } else {
            //String ayuda = tabla.getValueAt(filaseleccionada, num_columna).toString());
            String id = String.valueOf(vista.tablePilotos.getValueAt(filaseleccionada, 0));
            String Nombre = String.valueOf(vista.tablePilotos.getValueAt(filaseleccionada, 1));
            int trofeos = (int) vista.tablePilotos.getValueAt(filaseleccionada, 2);
            Object vehiculo = null;
            if (vista.tablePilotos.getValueAt(filaseleccionada, 4) != "no tiene") {

                System.out.println(vista.tablePilotos.getValueAt(filaseleccionada, 4));
                vehiculo = (int) vista.tablePilotos.getValueAt(filaseleccionada, 4);
                System.out.println(vehiculo);
                actualizarComboxVehiculo();
            } else {
                vista.dcbVehiculoPiloto.removeAllElements();
                vehiculo = "nombre: vacio null";
                actualizarComboxVehiculo();
                vista.dcbVehiculoPiloto.addElement("no tiene vehiculo");
                vista.dcbVehiculoPiloto.setSelectedItem((vista.dcbVehiculoPiloto.getElementAt(vista.dcbVehiculoPiloto.getIndexOf("no tiene vehiculo"))));
            }
            LocalDate Proxima_carrera = LocalDate.parse(String.valueOf(vista.tablePilotos.getValueAt(filaseleccionada, 3)));

            vista.textNombrePiloto.setText(Nombre);
            vista.textTrofeosPiloto.setText(String.valueOf(trofeos));
            vista.txtFechaPiloto.setDate(Proxima_carrera);
            for (int i = 0; i < vista.dcbVehiculoPiloto.getSize(); i++) {
                String cad = vista.dcbVehiculoPiloto.getElementAt(i).toString();
                String[] cadDefi = cad.split(" ");


                if (cadDefi[0].equals(String.valueOf(vehiculo))) {
                    vista.dcbVehiculoPiloto.setSelectedItem(vista.dcbVehiculoPiloto.getElementAt(i));
                }
            }
            vista.txtIdPiloto.setText(id);


        }
    }

    private void colocarDatosVehiculoEnTextField() {
        int filaseleccionada;

        //Guardamos en un entero la fila seleccionada.
        filaseleccionada = vista.tableVehiculo.getSelectedRow();
        if (filaseleccionada == -1) {
            JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna fila.");
        } else {
            //String ayuda = tabla.getValueAt(filaseleccionada, num_columna).toString());
            String Nombre = String.valueOf(vista.tableVehiculo.getValueAt(filaseleccionada, 1));
            String tipo = String.valueOf(vista.tableVehiculo.getValueAt(filaseleccionada, 4));
            String diseño = String.valueOf(vista.tableVehiculo.getValueAt(filaseleccionada, 5));
            Double precio = (double) vista.tableVehiculo.getValueAt(filaseleccionada, 2);
            String matricula = (String) vista.tableVehiculo.getValueAt(filaseleccionada, 3);

            vista.textNombreVehiculo.setText(Nombre);
            vista.textTipoVehiculo.setText(tipo);
            vista.textDiseñoVehiculo.setText(diseño);
            vista.textPrecioVehiculo.setText(String.valueOf(precio));
            vista.txtMatriculaVehiculo.setText(matricula);

        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getSource() == vista.textBusquedaGeneral) {
            if (vista.textBusquedaGeneral.getText().length() > 1) {
                int contador = 0;

                if (vista.circuitosCheckBox.isSelected()) {
                    ResultSet resultado = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "circuitos");
                    contador++;
                    try {
                        actualizarResulset(vista.dtmBusquedaCircuito,resultado);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                if (vista.vehiculoCheckBox.isSelected()) {
                    ResultSet resultado = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "vehiculo");
                    contador++;
                    try {
                        actualizarResulset(vista.dtmBusquedaVehiculo,resultado);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }

                }
                if (vista.pilotosCheckBox.isSelected()) {
                    ResultSet resultado = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "piloto");
                    contador++;
                    try {
                        actualizarResulset(vista.dtmBusquedaPilotos,resultado);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }

                }
                if (vista.circuitosCheckBox.isSelected() && vista.vehiculoCheckBox.isSelected() && vista.pilotosCheckBox.isSelected()){
                    ResultSet resultado = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "piloto");
                    contador++;
                    try {
                        actualizarResulset(vista.dtmBusquedaPilotos,resultado);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    ResultSet resultado1 = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "vehiculo");
                    contador++;
                    try {
                        actualizarResulset(vista.dtmBusquedaVehiculo,resultado1);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }

                    ResultSet resultado2 = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "circuitos");
                    contador++;
                    try {
                        actualizarResulset(vista.dtmBusquedaCircuito,resultado2);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }

            }
            if(contador==0){
                ResultSet resultado = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "piloto");
                contador++;
                try {
                    actualizarResulset(vista.dtmBusquedaPilotos,resultado);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                ResultSet resultado1 = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "vehiculo");
                contador++;
                try {
                    actualizarResulset(vista.dtmBusquedaVehiculo,resultado1);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                ResultSet resultado2 = modelo.buscarPorNombre(vista.textBusquedaGeneral.getText(), "circuitos");
                contador++;
                try {
                    actualizarResulset(vista.dtmBusquedaCircuito,resultado2);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }



            }
        }
        else if(e.getSource() == vista.txtTextoBusquedaPerosnalizada) {
            if (vista.txtTextoBusquedaPerosnalizada.getText().length() >= 1) {
                if (vista.txtCampoAbuscar.getText().length() != 0) {
                    String campo = vista.txtCampoAbuscar.getText();
                    if (vista.comboBoxBusquedaPersonalizada.getSelectedItem().equals("circuitos")) {

                        ResultSet resultado = modelo.buscarPersonalizado(vista.txtTextoBusquedaPerosnalizada.getText(), "circuitos", vista.txtCampoAbuscar.getText());
                        try {
                            actualizarResulset(vista.dtmBusquedaCircuito, resultado);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }

                    } else if (vista.comboBoxBusquedaPersonalizada.getSelectedItem().equals("pilotos")) {
                        ResultSet resultado = modelo.buscarPersonalizado(vista.txtTextoBusquedaPerosnalizada.getText(), "piloto", vista.txtCampoAbuscar.getText());

                        try {
                            actualizarResulset(vista.dtmBusquedaPilotos, resultado);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }

                    } else if (vista.comboBoxBusquedaPersonalizada.getSelectedItem().equals("vehiculos")) {

                        ResultSet resultado = modelo.buscarPersonalizado(vista.txtTextoBusquedaPerosnalizada.getText(), "vehiculo", vista.txtCampoAbuscar.getText());

                        try {
                            actualizarResulset(vista.dtmBusquedaVehiculo, resultado);
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }

                    }

                } else {
                    JOptionPane.showMessageDialog(null, "rellene campo de busqueda para realizar la busqueda",
                            "WARNING_MESSAGE", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

    }

    public void actualizarResulset(DefaultTableModel dlm, ResultSet resultado) throws SQLException {
        dlm.setRowCount(0);
        dlm.setColumnCount(0);
        ResultSet rs = null;

        rs=resultado;

            //Obteniendo la informacion de las columnas que estan siendo consultadas

        ResultSetMetaData rsMd = null;
        try {
            rsMd = rs.getMetaData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //La cantidad de columnas que tiene la consulta

        int cantidadColumnas = 0;
        try {
            cantidadColumnas = rsMd.getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Establecer como cabezeras el nombre de las colimnas

            for (int i = 1; i <= cantidadColumnas; i++) {
                try {
                    dlm.addColumn(rsMd.getColumnLabel(i));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            //Creando las filas para el JTable
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                dlm.addRow(fila);
            }
            rs.close();


    }




}


