package vista;

public class HiloUpdate extends Thread{
    Controlador controlador;
    Modelo modelo;
    Vista vista;
    int contador=0;

    public HiloUpdate(Vista vista, Modelo modelo, Controlador controlador) {
        this.controlador=controlador;
        this.modelo=modelo;
        this.vista=vista;
    }

    @Override
    public void run() {
        super.run();


        while (contador == 0){
            try {
                sleep(20000);
                System.out.println("llego "+ this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(contador==0) {
                controlador.actualizar(vista.defaultTablePilotos, "pilotos");
                controlador.actualizar(vista.defaultTableVehiculo, "vehiculos");
                controlador.actualizar(vista.dlmPilotosEnCirucito, "pilotosEnCircuitos");
                controlador.actualizar(vista.defaultTableCircuitos, "circuitos");
                controlador.actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
                controlador.actualizarJlistCircuitos(vista.dlmPilotos, "pilotos");
            }

        }
        System.out.println("fin hilo");


    }
    public void cambiarcontador(){
        contador=1;
    }
}
