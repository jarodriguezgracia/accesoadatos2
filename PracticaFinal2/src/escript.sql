create database final;

use final;

Create table vehiculos(
id int auto_increment primary key , 
nombre varchar(50),
precio REAL,matricula varchar(50) not null UNIQUE,
 tipo varchar(40),diseno varchar(40));
 
Create table pilotos(
id int auto_increment primary key,
nombre varchar(50),trofeos int,
proxima_carrera date,
vehiculo int,
FOREIGN KEY(vehiculo) references vehiculos(id) ON DELETE set null
);

Create table circuitos(id int auto_increment primary key,
nombre varchar(50) UNIQUE NOT NULL,
localidad varchar(50)
);

create table pilotos_circuito(
id_piloto int not null references pilotos(id),
id_circuito int  not null  references circuitos(id)
 );
 
select idCircuitos("juan")
 
delimiter $$
CREATE FUNCTION idCircuitos(nombreRecivido varchar(100)) RETURNS int 
BEGIN
declare respuesta int;
select id into respuesta from circuitos where circuitos.nombre=nombreRecivido;
return respuesta;
end$ 
delimiter ;$$

delimiter $$
create procedure consultar_Vehiculo()
begin
select * 
from vehiculos;
end;$$

delimiter $$
create procedure consultar_Circuitos()
begin
select * 
from circuitos;
end;$$


delimiter 
create procedure alta_Piloto_circuito(id_circuito int,id_piloto int)
begin
if((select count(*) from pilotos_circuito where pilotos_circuito.id_circuito=id_circuito and pilotos_circuito.id_piloto=id_piloto))>0 
then
select "Piloto ya añadido en circuito";
else
insert into pilotos_circuito(id_piloto,id_circuito) 
values (id_piloto,id_circuito);
end if;
end;

delimiter $$
create function consultar_pilotos(pid_circuito int)
returns int
begin
return(select count(*) from pilotos_circuito where id_circuito=pid_circuito);
end; $$

delimiter $$
create function consultar_circuitos(pid_piloto int)
returns int
begin
return(select count(*) from pilotos_circuito where id_piloto=pid_piloto);
end; $$

select consultar_circuitos(15);

delimiter $$
create procedure borrar_circuito(pnombre varchar(50))
begin
declare pid int(11);
if((select count(*) from circuitos where nombre like pnombre))=0 then
delete from circuitos where nombre like pnombre;
else
set pid = (select id from circuitos where nombre like pnombre);
delete from pilotos_circuito where id_circuito= pid;
delete from circuitos where id=pid;
end if;
end; $$


$$;
use final;
select circuitos.nombre,pilotos.nombre,vehiculos.nombre
from circuitos,pilotos,vehiculos,pilotos_circuito
where circuitos.nombre like '%' or pilotos.nombre like 's%' or vehiculos.nombre like 's%' and pilotos.vehiculo=vehiculos.id 
and pilotos_circuito.id_piloto=pilotos.id